class nginx {
    case $operatingsystem {
        debian, ubuntu: {
          file { '/etc/apt/trusted.gpg.d/nginx.gpg':
            ensure => file,
            owner => 'root',
            group => 'root', 
            mode => 0640,
            source => 'puppet:///modules/nginx/etc/apt/trusted.gpg.d/nginx.gpg',
          }

          $extension = inline_template("<%= operatingsystem.downcase %>")

          file { '/etc/apt/sources.list.d/nginx.list':
            ensure => file,
            owner => 'root',
            group => 'root', 
            mode => 0640,
            source => "puppet:///modules/nginx/etc/apt/sources.list.d/nginx.list.${extension}",
            require => File['/etc/apt/trusted.gpg.d/nginx.gpg'],
            notify => Exec['apt-update'],
          }
        }
        default: {
          err("${fqdn} runs ${operatingsystem}.")
          err("module nginx, is currently implemented for debian and ubuntu. If you need some other system, please go ahead and add it")
        }
      }
      
      package { [
                 'nginx'
                ]:
        ensure => latest, 
        require => Exec['apt-update'],    
      }

      
      file { [
              '/etc/nginx/sites-enabled',
              '/etc/nginx/sites-available',
             ]:
        ensure  => directory,
        owner => 'root', 
        group => 'root',
        mode  => '0744',
        require => Package['nginx'],
      }

      file { [
              '/etc/nginx/conf.d/default.conf',
              '/etc/nginx/conf.d/example_ssl.conf',
             ]:
        ensure  => absent,
        require => Package['nginx'],
      }


      #    ensure nginx is always running
      service { "nginx":
        ensure  => running,
        require => File['/etc/nginx/sites-available'],
      }
      
      file { "/etc/rc3.d/S99nginx":
        ensure  => link,
        path    => "/etc/rc3.d/S99nginx",
        target  => "/etc/init.d/nginx",
        require => Package['nginx'],
      }
}
